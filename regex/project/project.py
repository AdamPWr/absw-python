import re
import pyperclip

phoneReg = re.compile(r'''(\d{2})?          # optional dialing number
                          (-|\.|\s)?        #separator
                          (\d{3})           # 3 digits
                          (-|\.|\s)?        #separator
                          (\d{3})           # 3 digits
                          (-|\.|\s)?        #separator
                          (\d{3})           # 3 digits                            
                             ''', re.VERBOSE)

phones = phoneReg.findall("jdsfh 78-302 232 222  fsjfdn kdflng  43q9ji 473 212 111")
if phones:
    print(phones)
else:
    print("didn't match")

emailReg = re.compile(r'''[A-Za-z0-9+.-_%]+
                          @
                          [A-Za-z0-9]+
                          .
                          [a-zA-z]{2,4}
                          
                          
''', re.VERBOSE)

emails = emailReg.findall("jfn jfn adam.cierniak@gmail.com   jfdgndfj  adam.cierniak@gmail.comAndd  adam.cierniak@gmail.com. ")
if emails:
    print(emails)
else:
    print("didn't match")