import re

noNewlineRegex = re.compile('.*')
mo = noNewlineRegex.search('Serve the public trust.\n Protect the innocent.\n Uphold the law.')
if mo:
    print(mo.group())
else:
    print("didn't match")
newlineRegex = re.compile('.*', re.DOTALL)
mo = newlineRegex.search('Serve the public trust.\nProtect the innocent.\nUphold the law.')
if mo:
    print(mo.group())
else:
    print("didn't match")
