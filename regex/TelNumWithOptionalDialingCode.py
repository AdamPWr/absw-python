import re

reg = re.compile(r'(\d\d-)?\d\d\d-\d\d\d-\d\d\d')
mo = reg.search("Moj nr tel to 11-222-333-444")
if mo:
    print(mo.group())

mo = reg.search("Moj nr tel to 222-333-444")
if mo:
    print(mo.group())