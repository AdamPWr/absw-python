import re

batRegex = re.compile(r'Bat(wo)?man')

mo = batRegex.search("This is Batman")
if  mo:
    print(mo.group())

mo = batRegex.search("This is Batwoman")
if  mo:
    print(mo.group())

print("grops():",end="")
print(mo.groups())