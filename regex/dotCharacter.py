import re

#The . (or dot) character in a regular expression is called a wildcard and will
#match any character except for a newline. For example, enter the following
#into the interactive shell:

reg = re.compile(r'.at')
mo = reg.findall('Mat caf Kat massjkna jasnjat')
if mo:
    print(mo)
else:
    print('didn\'t match')

reg = re.compile(r'.*at')
mo = reg.findall('Mat caf Kat massjkna jasnjat')
if mo:
    print(mo)
else:
    print('didn\'t match')

