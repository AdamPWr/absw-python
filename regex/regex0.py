import re

pythonNumberRe = re.compile(r'\d\d\d-\d\d\d-\d\d\d')
mo = pythonNumberRe.search("Moj numer to 322-111-111")
if mo:
    print("result: "+mo.group())


print('Grupowanie nawiasami')
pythonNumberRe = re.compile(r'(\d\d\d)-(\d\d\d)-(\d\d\d)')
mo = pythonNumberRe.search("Moj numer to 322-111-111")
if mo:
    print("groups(): ",end="")
    print(mo.groups())
    print("group(0):", end="")
    print(mo.group(0))
    print("group(1):", end="")
    print(mo.group(1))
    print("group(2):", end="")
    print(mo.group(2))
    print("group(3):", end="")
    print(mo.group(3))
    print("group(4):", end="") #no such  group
    print(mo.group(4))