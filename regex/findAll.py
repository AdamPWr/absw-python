import re

reg = re.compile(r'\d+\s\w+')
mo = reg.findall("133 awf aff 33 w")
if mo:
    print(mo)
else:
    print('error')

reg2 = re.compile(r'[aeiouAEIOUY]')
mo = reg2.findall('Robocob eat baby food. BABY FOOD')
if mo:
    print(mo)
else:
    print('error')

reg2 = re.compile(r'[^aeiouAEIOUY]')
mo = reg2.findall('Robocob eat baby food. BABY FOOD')
if mo:
    print(mo)
else:
    print('error')