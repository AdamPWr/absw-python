import re

batRegex = re.compile(r'Bat(man|mobile|copter)')
mo = batRegex.search("To jest moj Batmobile")

if mo:
    print(mo.group())

mo = batRegex.search("To jest moj Batcopter")

if mo:
    print(mo.group())