import re

reg=re.compile(r'^Hello')
mo  = reg.search('Hello world')
if mo:
    print(mo.group())
else:
    print('error')

reg=re.compile(r'^Hello')
mo  = reg.search('World Hello')
if mo:
    print(mo.group())
else:
    print('error')

#The r'\d$' regular expression string matches strings that end with a
#numeric character from 0 to 9.

reg = re.compile(r'\d$')
mo = reg.search("sth 42")
if mo:
    print(mo.group())
else:
    print('didn \'t match')
mo = reg.search("42 sth")
if mo:
    print(mo.group())
else:
    print('didn \'t match')