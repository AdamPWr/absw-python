import shelve
import os

os.chdir("/home/adam/pythonExamples/filesAndFolders/")

shFile = shelve.open('myVariables')
cats = ['cat1','cat2', 'cat3']
shFile['cats']= cats
shFile.close()

shFile = shelve.open('myVariables')
cats = shFile['cats']
shFile.close()
print(cats)

shFile = shelve.open('myVariables')
print(shFile.keys())
print(shFile.values)